

flipper: flipper.c flipper.h
	gcc -o $@ $<


test: flipper testdata
	cat testdata | ./flipper -l 16 -n 4 -s 1,2,0,3
