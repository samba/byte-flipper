
typedef struct {
  int chunk_size; // How many bytes to read in each chunk
  int chunk_count; // How many chunks to read in each sequence
  int chunk_offset; // How many bytes to skip up-front
  int * chunk_order; // what order of output for the chunks
} Options;



char * substr(char * incoming, int offset, int len);

int count_commas(char * incoming, int len);

void parse_comma_sep(char* incoming, Options * result);
