#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "flipper.h"

#define false 0
#define true 1
#define DEBUG 1 



Options * parseOptions(int argc, char ** argv){
  Options * result = malloc(sizeof(Options));
  char opt;
  extern char* optarg;
  char * chunk_order_string;
  while((opt = getopt(argc, argv, "l:n:o:s:")) != -1){
    switch(opt){
      case 'l': result->chunk_size = atoi(optarg); break;
      case 'n': result->chunk_count = atoi(optarg); break;
      case 'o': result->chunk_offset = atoi(optarg); break;
      case 's': chunk_order_string = optarg; break;
    }
  }
  parse_comma_sep(chunk_order_string, result);
  return result;
}

int count_commas(char * incoming, int len){
  int num_commas = 0;
  int i = len -1;
  while(i--){
    if(incoming[i] == ',') num_commas ++;
  }
  return num_commas;
}

int parseInt(char * input, int start, int len){
  char * temp = malloc(len * sizeof(char));
  memset(temp, 0, len * sizeof(char));
  temp = strncpy(temp, input + start, len);
  return atoi(temp);
}


void parse_comma_sep(char* incoming, Options * result){
  int pos;
  int comma_count = 0;
  int incoming_length = strlen(incoming);
  int num_items = result->chunk_count;
  int last_comma = 0;

  result->chunk_order = malloc((num_items) * sizeof(int)); 

  for(pos = 0; pos < incoming_length; pos++){
    if(incoming[pos] == ',' && pos > 0){
      result->chunk_order[comma_count++] = parseInt(incoming, last_comma, pos - last_comma);
      last_comma = pos +1;
    }
  }
 
  // Some bytes remained
  if(pos > last_comma){
    result->chunk_order[comma_count++] = parseInt(incoming, last_comma, pos - last_comma);
  }

  if(DEBUG){
    for(pos = 0; pos < comma_count; pos++){
      printf("# sequence %d: %d\n", pos, result->chunk_order[pos]);
    }
  }
}

// Reset string content for the array
void truncate_strings(char* chunk_group[], int chunk_count, int chunk_size){
  int i;
  for(i = 0; i < chunk_count; i++){
    memset(chunk_group[i], 0, chunk_size);
  }
}

// Write the string to output (with debugging, if enabled)
void print_chunk(char* chunk_group[], int chunk_index, int chunk_size){
  if(strlen(chunk_group[chunk_index]))
    fwrite(chunk_group[chunk_index], chunk_size, 1, stdout);
}



int main (int argc, char ** argv){

  Options * opt = parseOptions(argc, argv);

  if(DEBUG){
    printf("# opt chunk_size %d\n", opt->chunk_size);
    printf("# opt chunk_count %d\n", opt->chunk_count);
    printf("# opt chunk_offset %d\n", opt->chunk_offset);
  }

  int i; // sentinel
  char* chunk_group[opt->chunk_count];

  // initialize the string array
  for(i = 0; i < opt->chunk_count; i++){
    chunk_group[i] = malloc(opt->chunk_size +1);
  }

  while(!feof(stdin)){
    truncate_strings(chunk_group, opt->chunk_count, opt->chunk_size +1);
    for(i = 0; !feof(stdin) && i < opt->chunk_count; i++){
      if(0 == (int) fread(chunk_group[i], opt->chunk_size, 1, stdin)) break;
      if(ferror(stdin) || feof(stdin)) break;
    }
    for(i = 0; i < opt->chunk_count; i++){
      print_chunk(chunk_group, opt->chunk_order[i], opt->chunk_size);
    }
  }

  return 0;
}


